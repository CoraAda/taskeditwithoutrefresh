from django import forms
from django.conf import settings
from .models import Task
from django.contrib.admin import widgets  



class FormTaskAdd(forms.ModelForm):
    name = forms.CharField(max_length=40, required=False)
    description = forms.CharField(widget=forms.Textarea, max_length=200, required=False)
    due_date = forms.DateField(required=False, input_formats=settings.DATE_INPUT_FORMATS )

    class Meta:
    	model = Task
    	fields = ('name', 'description', 'due_date')

  #   def clean_name(self):
		# name = self.cleaned_data.get('name')
		# if name is not None and not name :
		# 	self.add_error(
		# 		'name',
		# 		'Cant be empty'
		# 	)
		# return name
class FormName(forms.ModelForm):
	name = forms.CharField(max_length=40, required=False)

	class Meta:
		model = Task
		fields = ('name',)

	def clean_name(self):
		name = self.cleaned_data.get('name')
		if name is not None and not name :
			self.add_error(
				'name',
				'Cant be empty'
			)
		return name

class FormDescription(forms.ModelForm):
	description = forms.CharField(widget=forms.Textarea, max_length=200, required=False)

	class Meta:
		model = Task
		fields = ('description',)

	def clean_description(self):
		description = self.cleaned_data.get('description')
		if description is not None and not description :
			self.add_error(
				'description',
				'Cant be empty'
			)
		return description

class FormDueDate(forms.ModelForm):
	due_date = forms.DateField(required=True, input_formats=settings.DATE_INPUT_FORMATS )

	class Meta:
		model = Task
		fields = ('due_date',)

	def clean_due_date(self):
		due_date = self.cleaned_data.get('due_date')
		if due_date is not None and not due_date :
			self.add_error(
				'due_date',
				'Cant be empty'
			)
		return due_date
# widget=forms.widgets.DateInput(format="%m/%d/%Y")