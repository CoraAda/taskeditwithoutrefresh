# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('polls2', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='description_text',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='task',
            old_name='task_text',
            new_name='name',
        ),
        migrations.AddField(
            model_name='task',
            name='modification_date',
            field=models.DateField(default=datetime.datetime(2015, 12, 23, 11, 43, 29, 459834, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='task',
            name='due_date',
            field=models.DateTimeField(verbose_name=b'due date'),
        ),
        migrations.AlterField(
            model_name='task',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
