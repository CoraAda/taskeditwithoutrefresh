$(document).ready(function() {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    var csrftoken = getCookie('csrftoken');
    var csrfSafeMethod = function(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'inline';

function response(response, newValue) {
    console.log(response);
    console.log(newValue);
    if ( response.error ) {
        alert(response.error);
        $('.error').text(response.error + response.name);
    }
    else if ( response.success ) {
        alert(response.success);
        $('.success').text(response.success)
    }
}

$('.name').editable({
    type: 'text',
    url: '/',
    title: 'Enter name:',
    success: response,
});

$('.description').editable({
    type: 'text',
    url: '/',
    title: 'Enter description:',
    success: response,
});

$('.due_date').editable({
    type: 'text',
    url: '/',
    title: 'Enter due_date:',
    success: response,
   

});
});
