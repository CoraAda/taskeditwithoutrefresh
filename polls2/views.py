from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from .models import Task
from .forms import FormTaskAdd, FormName, FormDescription, FormDueDate

def index(request):
    return render(request, 'polls2/index.html')

def add_task(request):
    tasks = Task.objects.all()
    if request.method == 'POST':
        form = FormTaskAdd(request.POST)
        if form.is_valid():
            # task = form.save(commit=False)
            form.save()
            return render(request, 'polls2/task_list.html', {'task_list':tasks})
    else:        
        form = FormTaskAdd()
    return render(request, 'polls2/add_task.html', {'form':form})

def task_list(request):
    tasks = Task.objects.all()
    return render(request, 'polls2/task_list.html', {'task_list':tasks})

def del_task(request, pk):
    tasks = get_object_or_404(Task, pk = pk)
    tasks.delete()
    return HttpResponseRedirect("/tasks/")

def edit_task(request, pk):
    tasks = Task.objects.all()
    task = Task.objects.get(pk=pk)
    if request.POST:
        form=FormTaskAdd(request.POST,instance=task)
        if form.is_valid():
            form.save()
            return render(request, 'polls2/task_list.html', {'task_list':tasks})
    else:
        form=FormTaskAdd(instance=task)
    return render(request, 'polls2/task_edit.html', {'form':form, 'task': task})

def edit_without_refresh(request):
    if request.POST:
        task = get_object_or_404(Task, pk = request.POST.get('pk'))
        field_name = request.POST.get('name')
        field_value = request.POST.get('value')
        data = {field_name: field_value }
        form=FormTaskAdd( data  ,instance=task)
        #1 form_name = FormName(data, instance=task)
        #1 form_description = FormDescription(data, instance=task)
        #1 form_duedate = FormDueDate(data, instance=task)
        #1 if field_name == 'name' or field_name =='description' or field_name == 'due_date':
        if form.is_valid() and field_value !='':
            form.save()
            return JsonResponse({'success': ' Yesss! '}) 
        else:
            errors = dict((k, map(unicode, v))for (k,v) in form.errors.items())
            print errors
            errors.update({'error': ' Nooo! '})
            errors 
            return JsonResponse(errors)
    else:
        form=FormTaskAdd(instance=task)
    return HttpResponse()




    #1         if form_name.is_valid():
    #             form.save()
    #             return JsonResponse({
    #                 'success': ' Yesss! '
    #             }) 
    #         else:
    #             errors = dict(
    #                 (k, map(unicode, v))
    #                 for (k,v) in form.errors.items()
    #             )
    #             print errors
    #             errors.update({'error': ' Nooo! '})
    #             errors 
    #             return JsonResponse(errors)
    #     elif field_name == 'description':
    #         if form_description.is_valid():
    #             form.save()
    #             return JsonResponse({
    #                 'success': ' Yesss! '
    #             }) 
    #         else:
    #             errors = dict(
    #                 (k, map(unicode, v))
    #                 for (k,v) in form.errors.items()
    #             )
    #             print errors
    #             errors.update({'error': ' Nooo! '})
    #             errors 
    #             return JsonResponse(errors)
    #     elif field_name == 'due_date':
    #         if form_duedate.is_valid():
    #             form.save()
    #             return JsonResponse({
    #                 'success': ' Yesss! '
    #             }) 
    #         else:
    #             errors = dict(
    #                 (k, map(unicode, v))
    #                 for (k,v) in form.errors.items()
    #             )
    #             print errors
    #             errors.update({'error': ' Nooo! '})
    #             errors 
    #             return JsonResponse(errors)
    # else:
    #     form=FormTaskAdd(instance=task)
    #1 return HttpResponse()